import React, { Component } from 'react';
import '../../styles/app.css';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import TrainDetails from './TrainDetails';
import FindTrain from './FindTrain';
import listOfTrains from '../../data/listOfTrain';
import SpecTrainDetails from './SpecTrainDetails';
import UserDetails from './UserDetails';
import ConfirmationPage from './ConfirmationPage';

class TrainReservation extends Component {
  constructor(props) {
    super();
    this.state = {
      trains: props.trains,
      searchDetails: {},
      specTrainDetails: {},
      userDetails: {},
    }
    this.searchDetail=this.searchDetail.bind(this);
    this.changeNoOfSeat=this.changeNoOfSeat.bind(this);
    this.userDetails=this.userDetails.bind(this);
    this.specTrainDetails=this.specTrainDetails.bind(this);
  }
 
  searchDetail(data) {
    this.setState({
      searchDetails: {
        from: data.from,
        to:data.to,
      },
      bookTrain:data.bookTrain,
      
    })
  }

  specTrainDetails(train) {
    this.setState({
      specTrainDetails: train,
    })
  }

  userDetails(data) {
    this.setState({
      userDetails: data,
    });
  }

  changeNoOfSeat(data) {
    const trainUpdate = [...this.state.trains];
    data.available_seat = data.available_seat -1;
    trainUpdate.splice((data.id - 1), 1, data);
    this.setState({
      trains: trainUpdate,
      specTrainDetails: data,
    });
  }

  render() {
    const listOfTrains = this.state.trains.filter(item => item.from == this.state.searchDetails.from
      && item.to == this.state.searchDetails.to)
    return (
      <Router>
        <div className="trainReservation">
          <Switch>
            <Route path="/" exact render={(props)=> <FindTrain {...props} searchDetail={this.searchDetail}/>} />
            <Route path="/trains/" exact render={(props)=> <TrainDetails {...props} listOfTrains={listOfTrains} specTrainDetails={this.specTrainDetails} searchDetails={this.state.searchDetails}/>} />
            <Route path="/bookTrain/" exact render={(props)=> <SpecTrainDetails {...props} details={this.state.specTrainDetails} changeNoOfSeat={this.changeNoOfSeat}/>} />
            <Route path="/bookTrain/userDetails/" exact render={(props)=> <UserDetails {...props} userDetails={this.userDetails}/>} />
            <Route path="/bookTrain/confirmationPage/" exact render={(props)=> <ConfirmationPage {...props} userDetails={this.state.userDetails} details={this.state.specTrainDetails}/>} />
          </Switch>
        </div>
      </Router>
    );
  }
}
TrainReservation.defaultProps = listOfTrains;

export default TrainReservation;
