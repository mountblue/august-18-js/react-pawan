import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Rail from '../../railway.jpg';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';

class TrainDetails extends Component {
  constructor(props) {
    super();
  }

  bookTrain(train) {
    this.props.specTrainDetails(train);
    this.props.history.push("/bookTrain/");
  }

  render() {
    const listOfTrain = this.props.listOfTrains.map((train) => {
      return (
        <ListGroupItem key={train.id} >{train.trainName}
          <Button bsStyle="primary" onClick={() => this.bookTrain(train)}>Book Train</Button>
        </ListGroupItem>
      );
    });
    return (
      <div className="trainDetails">
        <img src={Rail} />
        <h2>List of Train From: {this.props.searchDetails.from} To : {this.props.searchDetails.to}</h2>
        <div className="listOfTrain">
          <Link to="/"><h3>Home</h3></Link>
          <ListGroup>
            {listOfTrain}
          </ListGroup>
        </div>
      </div>
    );
  }
}

export default TrainDetails;
