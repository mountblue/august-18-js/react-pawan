import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Rail from '../../railway.jpg';
import { Button, ControlLabel, FormControl, FormGroup, HelpBlock  } from 'react-bootstrap';
import { FieldGroup } from 'react-bootstrap'
import { form } from 'react-bootstrap'

class UserDetails extends Component {
  constructor(props) {
    super();
    this.state = {
      name:'',
      age:'',
      gender:'',
      userDetails:{},
    }
    this.confirmBooking = this.confirmBooking.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleGender = this.handleGender.bind(this);
    this.handleAge = this.handleAge.bind(this);
  }

  handleName(event){
    event.preventDefault();
    this.setState({
      name:event.target.value,
    })
  }

  handleAge(event){
    event.preventDefault();
    this.setState({
      age:event.target.value,
    })
  }

  handleGender(event){
    event.preventDefault();
    console.log('clicked');
    this.setState({
      gender:event.target.value,
    })
  }

  confirmBooking(event) {
      event.preventDefault();
      console.log(this.state.name);
      this.setState({
          userDetails: {
              userName:this.state.name,
              gender:this.state.gender,
              age:this.state.age,
          },
      }, function() {
        console.log(this.state.userDetails);
        this.props.userDetails(this.state.userDetails);
        this.props.history.push("/bookTrain/confirmationPage/");
      })
  }

  render() {
    return (
      <div className="userDetails">
        <img src={Rail} />
        <h2>Booking details</h2>
        <form>
          <Link to="/">Home</Link>
          <h4>Enter your details</h4><br/>
          <div className="userName">
            <ControlLabel>Name:</ControlLabel>
            <FormControl id="formControlsText" type="text" placeholder="Enter name"onChange={this.handleName}/>
          </div><br />
          <div className="age">
            <ControlLabel>Age:</ControlLabel>
            <FormControl id="formControlsText" type="text" placeholder="Enter age" onChange={this.handleAge}/>
          </div><br />
          <div className="gender">
            <ControlLabel>Gender:</ControlLabel>
            <FormControl componentClass="select" onChange={this.handleGender}>
              <option value="select">Select gender</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </FormControl>
          </div><br />
          <Button bsStyle="primary" onClick={this.confirmBooking}>Confirm Booking</Button>
        </form>
      </div>
    );
  }
}

export default UserDetails;
