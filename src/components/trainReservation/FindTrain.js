import React, { Component } from 'react';
import Rail from '../../railway.jpg';
import { Button, FormControl, ControlLabel } from 'react-bootstrap';


class FindTrain extends Component {
  constructor(props) {
    super();
    this.state = {
      stations: props.stations,
      findTrain: {},
      from:'',
      to:'',
    };
    this.findTrain = this.findTrain.bind(this);
    this.handleFrom = this.handleFrom.bind(this);
    this.handleTo = this.handleTo.bind(this);
  }

  handleFrom(event) {
    event.preventDefault();
    this.setState({
      from: event.target.value,
    })
  }

  handleTo(event) {
    event.preventDefault();
    this.setState({
      to: event.target.value,
    })
  }

  findTrain(event) {
    event.preventDefault();
    this.setState({
      findTrain: {
        from: this.state.from,
        to: this.state.to,
      }, 
    }, function() {
      this.props.searchDetail(this.state.findTrain);
      if(this.state.findTrain.from != this.state.findTrain.to){
        this.props.history.push("/trains/");
      } 
    }); 
  }

  render() {
    const stationOption = this.state.stations.map((station) => {
      return <option key={station} value={station}>{station}</option>;
    }); 
    return (
      <div className="findTrain">
        <img src={Rail} />
        <h2>Railway Reservation System</h2>
        <form>
          <h3>BOOK</h3>
          <h4>YOUR TICKET</h4><br />
          <div className="from">
            <ControlLabel>Station From:</ControlLabel>
            <FormControl componentClass="select" onChange={this.handleFrom}>
              <option value="select">select station</option>
              {stationOption}
            </FormControl>
          </div><br />
          <div className="to">
            <ControlLabel>Station To:</ControlLabel>
            <FormControl componentClass="select" onChange={this.handleTo}>
              <option value="select">select station</option>
              {stationOption}
            </FormControl>
          </div><br />
          <Button bsStyle="primary" onClick={this.findTrain}>Find Train</Button>
        </form>
      </div>
    );
  }
}
FindTrain.defaultProps = {
  stations: ['patna', 'delhi', 'bangaluru', 'kolkata'],
};

export default FindTrain;
