import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Rail from '../../railway.jpg';
import { Button } from 'react-bootstrap';

class SpecTrainDetails extends Component {
  constructor(props) {
    super(props);
    this.continueBooking = this.continueBooking.bind(this);
  }

  continueBooking() {
    if (this.props.details.available_seat > 0) {
      this.props.changeNoOfSeat(this.props.details);
      this.props.history.push("/bookTrain/userDetails/");
    }
  }

  render() {
    return (
      <div className="specTrainDetails">
        <img src={Rail} />
        <h2>Train details of : {this.props.details.trainName}</h2>
        <div className="details">
          <Link to="/"><h3>Home</h3></Link>
          <h3>From: {this.props.details.from}</h3>
          <h3>To: {this.props.details.to}</h3>
          <h3>Train number: {this.props.details.id}</h3>
          <h3>Train name: {this.props.details.trainName}</h3>
          <h3>Available_seat: {this.props.details.available_seat}</h3>
          <Button bsStyle="primary" onClick={this.continueBooking}>Continue Booking</Button>
        </div>
      </div>
    );
  }
}

export default SpecTrainDetails;
