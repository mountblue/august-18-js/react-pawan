import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Rail from '../../railway.jpg';

class ConfirmationPage extends Component {
  constructor(props) {
    super();
  }

  render() {
    return (
        <div className="confirmationPage">
            <img src={Rail} />
            <h2>Ticket confirmationPage</h2>
            <div className="details">
                <Link to="/"><h3>Home</h3></Link>
                <h4>Your ticket is confirm booked</h4><br />
                <h4>User details</h4>
                <p>User name: {this.props.userDetails.userName}</p>
                <p>Age: {this.props.userDetails.age}</p>
                <p>Gender: {this.props.userDetails.gender}</p>
                <h4>Train details</h4>
                <p>From: {this.props.details.from}</p>
                <p>To: {this.props.details.to}</p>
                <p>Train number: {this.props.details.id}</p>
                <p>Train name: {this.props.details.trainName}</p>
            </div>
        </div>
    );
  }
}

export default ConfirmationPage;
