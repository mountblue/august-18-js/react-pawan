import React, { Component } from 'react';
import '../styles/app.css';
import TrainReservation from './trainReservation/TrainReservation';

class App extends Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <div className="app">
        <TrainReservation />
      </div>
    );
  }
}

export default App;
